const { conectarMongo } = require('./BaseDeDatos')
const trigger_cantidadfarmaco = require('./Triggers/trigger_cantidad_farmaco')

// Datos de la inserción del DetalleVenta
const detalle_venta = {
  detav_id: 28,
  venta_id: 1,
  cli_id: 2,
  farmaco_id: 1,
  ven_id: 1,
  detav_cantidad: 60
}

// Insertar
const insertar = async () => {

  await conectarMongo()

  // Trigger
  trigger_cantidadfarmaco(detalle_venta)

}

insertar()